function Main(doc, win) {
    this.w = win;
    this.d = doc;
    this.tag = 'tags=';
}

Main.prototype = {
    updateList: function (hashChanged) {
        var hash = this.w.location.hash.substring(1).replace(this.tag, '');
        var array = hash.split(",").map(String);
        var ul = this.getEl(this,'colors-list');
        var items = this.d.querySelectorAll('.colors-list li');

        if(hashChanged) {
            for (var i = 0, li = null; i < items.length; ++i) {
                li = items[i];
                li.parentNode.removeChild(li);
            }
        }

        for (var i = 0, li = null; i < array.length; ++i) {
            li = this.d.createElement("li");
            li.addEventListener('click', this.onRemoveClick.bind(undefined, this, i));
            li.appendChild(this.d.createTextNode(array[i]));
            li.setAttribute("class", "colors-list__item");
            ul.appendChild(li);
        }
    },
    getEl: function(self, name) {
        return self.d.getElementsByClassName(name)[0];
    },
    onAddClick: function(self, e) {
        var value = self.getEl(self,'colors-menu__input').value;
        if(value != '') {
            self.w.location.href = self.w.location.href + ',' + value;
            self.getEl(self,'colors-menu__input').value = '';
        }
    },
    onRemoveClick: function(self, index, e) {
        var hash = self.w.location.hash.substring(1).replace(self.tag, '');
        var array = hash.split(",").map(String);
        array.splice(index, 1);
        self.w.location.hash = self.tag + array.join();
    },
    init: function (self) {
        var hashChanged = false;
        this.w.addEventListener('hashchange', function (e) {
            hashChanged = true;
            self.updateList(hashChanged);
        });

        this.getEl(self,'colors-menu__button').addEventListener('click', this.onAddClick.bind(undefined, self));
        this.w.location.hash = 'tags=red,blue,purple';
        this.updateList(false);
    }
}